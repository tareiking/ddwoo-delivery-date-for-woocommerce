<?php 

/*
Plugin Name: DDWoo: Delivery Date for Woocommerce 
Plugin URI: http://dev.tripleshotcreative.com.au/DDWoo
Description: This plugin adds a Delivery Date Picker for WooCommerce on Checkout. Built atop Ashok Rane's work at http://www.tychesoftwares.com/about
			 Uses glDatePicker jQuery plugin by http://glad.github.io/
Author: Tarei King
Version: 0.5
Author URI: http://github.com/tareiking
Contributor: Tripleshot Creative, http://www.tripleshotcreative.com.au
*/

/*

	TODO:
	- Add optional WooCommerce action hooks to determine where the date picker is shown
	- Add custom before / after filters to add HTML / markup to the date picker
	- Buy a beer
	- Add screenshots
*/


$wpefield_version = '0.5';

add_action('woocommerce_after_checkout_billing_form', 'my_custom_checkout_field'); 

function my_custom_checkout_field( $checkout ) {	

    wp_enqueue_style( 'datepicker', plugins_url('css/glDatePicker.default.css', __FILE__) , '', '', true);
    wp_enqueue_script( 'glDatePicker', plugins_url('js/glDatePicker.min.js', __FILE__), '', '', true);


    //  Load the HTML for form item
	woocommerce_form_field( 'ddwoo_deliverydate', 
		array(        
			'type'          	=> 'text',
			'label'         	=> __('Delivery Date'),		
			'required' 	 		=> false,	
			'placeholder'   	=> __('Delivery Date'),        
		), 

		$checkout->get_value( 'ddwoo_deliverydate' ));     


	echo '<div gldp-el="mydate"
         	style="width:400px; height:300px; position:absolute; top:70px; left:100px;">
    	 </div>';
	echo '<script language="javascript">
	        $(window).load(function()
	        {
	            $(\'#ddwoo_deliverydate\').glDatePicker();
	        });
		  </script>';
}

add_action('woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta'); 

function my_custom_checkout_field_update_order_meta( $order_id ) {    

	if ($_POST['ddwoo_deliverydate']) {

		update_post_meta( $order_id, 'Delivery Date', esc_attr($_POST['ddwoo_deliverydate']));

	}
	
}

?>